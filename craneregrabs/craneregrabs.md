Modification to the CFO's cranes to make them able to grab objects again while they're dropping. 

This behaviour matches the behaviour of the cranes before the April 25th 2007 update.